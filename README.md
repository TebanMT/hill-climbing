# Bibliotecas de Metaheuristicas

En este proyecto se implementan varias metaheuristicas para la solucion de tres problemas especificos: *problema de la mochila*, *agente viajero* y *optimizacion de funciones*.
El problema de la mochila (solucionado con Ascenso de colinas) cuenta con interfaz grafica, uno de los objetivos es que todos los problemas cuenten con interfaz grafica (web). Actualmente todos los algoritmos son funcionales y se pueden utilizar a conveniencia.

## Metaheuristicas Implementadas
* [Ascenso de colinas](https://es.wikipedia.org/wiki/Algoritmo_hill_climbing) <- Aleatorio
* [Recocido Simulado](https://es.wikipedia.org/wiki/Algoritmo_de_recocido_simulado)
* [Algoritmos Geneticos](https://es.wikipedia.org/wiki/Algoritmo_gen%C3%A9tico)

## Construido con 🛠️

* [Python](https://www.python.org/)
* [Node js](https://nodejs.org/es/)
* [Anime js](https://animejs.com/) - Biblioteca para las animaciones en la pagina web

## Demo
* [Demo](https://hill-climbig.herokuapp.com/)

## ¿Cómo usar el demo?
* 1.- Arrastrar la imagen del item a la canasta.
* 2.- Cada que *coloque* el item a la canasta, la pagina pedira que ingrese el
peso y el valor del item. Despues, el item cambiara.
* 3.- Solo hay 10 imagenes diferentes, sin embargo el ultimo item es de un
infinito, este item se puede ingresar infinitas veces.
* 4.- El boton *ver items* abrira una ventana modal donde se veran todos los
items dentro del carrito.
* 5.- El boton *Ejecutar* enviara los datos al servidor y despues a un script de
python que ejecutara el algoritmo y rotarnara el resultado.
* 6.- Despues de ejecutar el algoritmo se activara el boton *reiniciar*, el peso y
el valor se mostraran en la grafica y los items que resuelven el problema
se puden ver si se da click a la mochila y los items restantes del carrito en
el boton *ver items*.
###### Nota
Cerrar los modales con los botones

## Estado
#### Finalizado con continuidad

## License
[MIT](https://gitlab.com/TebanMT/hill-climbing/-/blob/master/LICENSE)

## Imagenes de Ejemplo
![alt text](./public/img/demo.png)
