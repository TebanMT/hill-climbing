from numpy.random import randint

class Ciudad(object):

    def __init__(self, *args, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)

    def get(self, k, default=None):
        try:
            return getattr(self,k)
        except AttributeError:
            return default

class Viajero(object):

    def __init__(self, matrix=[], max_iter=0, *args, **kwargs):
        self.matrix = matrix
        self.max_iter = max_iter

    def initial_solution(self, ciudad_origen = 0):
        solution = []
        ciudades = [i for i in range(len(self.matrix))]
        #solution.append(ciudad_origen)
        ciudades.remove(ciudades[ciudad_origen])
        while len(ciudades) != 0:
            random_index = randint(len(ciudades), size=1)[0]
            solution.append(ciudades[random_index])
            ciudades.remove(ciudades[random_index])
        #solution.append(ciudad_origen)
        return solution

    def eval(self, solution):
        length = 0
        for i in range(len(solution)):
            length += self.matrix[solution[i-1]][solution[i]]
        return length

    def random_neighbour(self, solution):
        partial_solution = solution[1:-1]
        random_index = randint(len(partial_solution), size=2)
        partial_solution[random_index[0]], partial_solution[random_index[1]] = partial_solution[random_index[1]], partial_solution[random_index[0]]
        return solution[:1]+partial_solution+solution[-1:]

    def hill_climb(self):
        solution = self.initial_solution()
        solution_eval = self.eval(solution)
        for i in range(self.max_iter):
            neighbour = self.random_neighbour(solution.copy() )
            neighbour_eval = self.eval(neighbour)
            message = str(i+1)+'.- Candidato: '+str(neighbour)+ ' con Distancia: '+str(neighbour_eval)+ ' < a solucion actual: '+str(solution)+ ' y distancia: '+str(solution_eval)
            if neighbour_eval < solution_eval:
                solution, solution_eval = neighbour, neighbour_eval
                message += ' --> El candidato es mejor solucion'
            print(message)
        return (solution, solution_eval)

    def solve_ga(self, num_individuos, num_generaciones=100):
        import algos_geneticos as g
        def eval(*args):
            length = 0
            for i,v in enumerate(args):
                length += self.matrix[args[i-1]][args[i]]
            return length
        res = []
        evaluacion = 0
        poblacion = g.Poblacion(num_individuos, len(self.matrix[0]),limites_inf=0, limites_sup=len(self.matrix[0]), repre=self.initial_solution, verbose=True)
        poblacion.optimizar(eval, num_generaciones=num_generaciones, tipo_mutacion="intercambio")
        res.append(0)
        res = res + poblacion.cromosoma_optimo
        res.append(0)
        evaluacion = self.eval(res)
        return (res,evaluacion)
