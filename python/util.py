import functools
import time
import numpy as np

def write_specific_line(ruta="/home/estebanmendiola/Documents/Metaheuristicas/Practicas/Practica 1/codigo/public/reports/informe.txt", linea=0, text=''):
    """Escribe texto en una linea specifica de un archivo

    Parameters
    ----------
    ruta  : str Ruta del archivo
    linea : int Linea donde escribir
    text  : str Texto a escribir

    Returns
    -------
    None
    """
    with open(ruta, 'r') as file:
        data = file.readlines()
    data[linea] = text
    with open(ruta, 'w') as file:
        file.writelines( data )

def txt_decorator(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        f = open("/home/estebanmendiola/Documents/Metaheuristicas/Practicas/Practica 1/codigo/public/reports/informe.txt", "w")
        f.write("-------RESUMEN---------\n\nPeso maximo de la mochila: "+str(args[0].max_peso)+"\nNumero de Iteraciones: "+str(args[0].max_iter)+"\nCantidad de Items: "+str(len(args[0].items)))
        kwargs['file'] = f
        start_time = time.time()
        result = func(*args, **kwargs)
        finish_time = time.time()
        f.close()
        write_specific_line(linea=8, text='Solucion Final: '+str(result['solucion_binary'])+'\nNumero de Items: '+str(len(result['solucion_items']))+'\nPeso Final: '+str(result['peso'])+'\nValor Final: '+str(result['valor'])+'\nTiempo de ejecución: '+str((finish_time - start_time))+' Segundos\n\n')
        return result
    return wrapper


def txt_decorator_funcion(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        f = open("./public/reports/informe_funcion.txt", "w")
        f.write("-------RESUMEN---------\n\nIntervalo: "+str(args[0].intervalo)+"\nNumero de Iteraciones: "+str(args[0].max_iter)+"\nCantidad de x´s: "+str(args[0].D))
        kwargs['file'] = f
        start_time = time.time()
        result = func(*args, **kwargs)
        finish_time = time.time()
        f.close()
        write_specific_line(ruta="./public/reports/informe_funcion.txt", linea=7, text='Solucion Final x='+str(result[0])+'\nf(x)='+str(result[1])+'\nTiempo de ejecución: '+str((finish_time - start_time))+' Segundos\n\n')
        return result
    return wrapper


def statistic_decorator_funcion(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        res = """ \\begin{table}[h]
                  \\centering
                  \\begin{tabular}{|l|l|l|}
                  \\hline
                  \\multicolumn{3}{|c|}{Para D=10 Valores de la funcion Estacional}                \\\ \\hline
                  Funcion & Mejor & Desviación Estandar \\\ \\hline"""

        res2 = """ \\begin{table}[h]
                   \\centering
                  \\begin{tabular}{|l|l|l|}
                  \\hline
                  \\multicolumn{3}{|c|}{Para D=10 Valores de Tiempo Estacional}                \\\ \\hline
                  Funcion & Mejor & Desviación Estandar \\\ \\hline"""
        for i,x in enumerate(result):
            values_x = list((i[0] for i in x))
            values_time = list((i[1] for i in x))
            #print("--------------------f"+str(i+1)+"------------------")
            #print("Best_values = "+str(find_nearest(values_x,0)))
            #print("Best_time = "+str(find_nearest(values_time,0)))
            #print("worst_values = "+str(find_farthest(values_x,0)))
            #print("worst_time = "+str(find_farthest(values_time,0)))
            #print("average_values = "+str(average(values_x)))
            #print("average_time = "+str(average(values_time)))
            #print("median_values = "+str(median(values_x)))
            #print("median_time = "+str(median(values_time)))
            #print("standart_dev_values = "+str(standart_dev(values_x)))
            #print("standart_dev_time = "+str(standart_dev(values_time)))
            #print("----------------------------------------------------")
            res += """ f{} & {:.2f} & {:.2f} \\\ \\hline
                """.format((i+1),find_nearest(values_x,0),standart_dev(values_x) )
            res2 += """ f{} & {:.2f} & {:.2f}  \\\ \\hline
                """.format((i+1),find_nearest(values_time,0),standart_dev(values_time) )
        res += """\\end{tabular}
\\end{table}"""
        res2 += """\\end{tabular}
\\end{table}"""
        print(res)
        print("")
        print("")
        print(res2)
        return result
    return wrapper


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

def find_farthest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmax()
    return array[idx]

def average(array):
    res = sum(array) / len(array)
    return res

def median(array):
    array.sort()
    if len(array) % 2 != 0:
        n = len(array)
        mediana = (array[n // 2 - 1] + array[n // 2]) / 2
    else:
        mediana = array[len(array) // 2]
    return mediana

def standart_dev(array):
    import statistics
    return statistics.stdev(array)
