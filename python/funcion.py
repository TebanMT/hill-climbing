# Copyright Esteban Mendiola Tellez All Rights Reserved.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file at root of this proyect or in
# https://gitlab.com/TebanMT/hill-climbing/-/blob/master/LICENSE
# ==============================================================================
# hill climbing search of a one-dimensional objective function
import numpy as np
from numpy.random import randint, uniform

from util import txt_decorator_funcion, statistic_decorator_funcion

class HillClimbing():

    def __init__(self, max_iter, intervalo, D, function):
        self.max_iter = max_iter
        self.intervalo = intervalo
        self.D = D
        self.function = function

    def initial_solution(self):
        res = [np.round(uniform(low=self.intervalo[0], high=self.intervalo[1]),5) for _ in range(self.D)]
        return res

    def random_neighbour(self, solution):
        random_index = randint(len(solution), size=1)[0]
        solution[random_index] = np.round(uniform(low=self.intervalo[0], high=self.intervalo[1]),5)
        return solution

    def eval(self, solution):
        def __getitem__(array, key):
            try:
                return array[key]
            except IndexError:
                return 0
        res = eval(self.function,{"__getitem__":__getitem__,"solution":solution,"np":np})
        return res

    #@txt_decorator_funcion depreciado: por falta mantenimiento para multiples reportes de las multiples ejecuciones
    def solve(self, *args, **kwargs):
        #f = kwargs['file']
        solution = self.initial_solution()
        solution_eval = self.eval(solution)
        #f.write("\nSolucion Inicial x="+str(solution)+"\nf(x)="+str(solution_eval)+'\n\n===========ITERACIONES==============\n\n')
        for _ in range(self.max_iter):
            neighbour = self.random_neighbour(solution.copy())
            neighbour_eval = self.eval(neighbour)
            #message = str(i+1)+'.- Candidato x: '+str(neighbour)+ ' con f(x)={:.5f}'.format(neighbour_eval)+ ' < a solucion actual x: '+str(solution)+ ' con f(x)={:.5f}'.format(solution_eval)
            if neighbour_eval < solution_eval:
                solution, solution_eval = neighbour, neighbour_eval
                #message += ' --> El candidato es mejor solucion'
            #message += '\n'
            #f.write(message)
        return [solution, solution_eval]

    def solve_sa(self, *args, **kwargs):
        import numpy as np
        solution = self.initial_solution()
        solution_eval = self.eval(solution)
        temperatura = solution_eval * 0.4
        while temperatura >= 0.1:
            for _ in range(self.max_iter):
                candidato = self.random_neighbour(solution.copy())
                candidato_eval = self.eval(candidato)
                delta =  candidato_eval - solution_eval
                if np.random.uniform(low=0.0, high=1.0, size=None) <= np.power(np.e,(-delta/temperatura)) or delta < 0:
                    solution, solution_eval = candidato, candidato_eval
            temperatura = temperatura * np.random.uniform(low=0.8, high=0.99, size=None)
        return [solution, solution_eval]

    def solve_ga(self,num_individuos, num_generaciones=100):
        import algos_geneticos as g
        def evaluacion(*solution):
            def __getitem__(array, key):
                try:
                    return array[key]
                except IndexError:
                    return 0
            res = eval(self.function,{"__getitem__":__getitem__,"solution":list(solution),"np":np})
            return res
        poblacion = g.Poblacion(num_individuos, self.D, limites_inf=self.intervalo[0], limites_sup=self.intervalo[1], repre=self.initial_solution, verbose=False)
        poblacion.optimizar(evaluacion, optimizacion="minimizar", num_generaciones=num_generaciones, tipo_mutacion="uniforme", cruzamiento="uniforme", elitismo=0.2, stop_temprano=True, rondas_tolerancia=10)
        return [poblacion.cromosoma_optimo,poblacion.valor_funcion_optimo]



@statistic_decorator_funcion
def execute(functions):
    import time
    for f in functions:
        h = HillClimbing(500, [-10,10],30, f)
        statistic = []
        for _ in range(20):
            start_time = time.time()
            s = h.solve()
            finish_time = time.time()
            statistic.append((s[1], finish_time - start_time))
        yield statistic

functions = [
    #|𝑥𝑖sin(𝑥𝑖)+0.1𝑥𝑖|
    "sum(map(lambda x: abs(x*np.sin(x)+0.1*x),solution))",
    #(𝑥1−1)^2+  𝑖(2sin(𝑥𝑖)−𝑥𝑖−1)^2
    "(solution[0]-1)**2 + sum(map(lambda args: args[0]*(2*np.sin(args[1])-solution[args[0]-1])**2, enumerate(solution[1:])))",
    #|𝑥𝑖^5−3𝑥𝑖^4+4𝑥𝑖^3−2𝑥𝑖^2−10𝑥_1−4|
    "sum(map(lambda x: abs((x**5)-(3*x**4)+(4*x**3)-(2*x**2)-(10*solution[1])-4), solution))",
    #x^10
    "sum(map(lambda x: x**10, solution))",
    #
    "sum(map(lambda args: (__getitem__(solution,args[0]+1)**2 + args[1]**2)**0.25 * (np.sin(50*(__getitem__(solution,args[0]+1)**2+args[1]**2)**0.1)**2 +0.1), enumerate(solution)))",
    #ix^2
    "sum(map(lambda args: args[0]*args[1]**2, enumerate(solution)))"]
execute(functions)