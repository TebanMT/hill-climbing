# Copyright Esteban Mendiola Tellez All Rights Reserved.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file at root of this proyect or in
# https://gitlab.com/TebanMT/hill-climbing/-/blob/master/LICENSE
# ==============================================================================

import numpy as np
import random
import copy
import time
from datetime import datetime

class Individuo:

    def __init__(self, num_genes, limite_sup=None, limite_inf=None, repre="discreta", verbose = False):
        self.num_genes = num_genes
        self.fitness = None
        self.valor_funcion = None
        if limite_inf is isinstance(limite_inf,list) and limite_sup is isinstance(limite_sup,list):
            if len(limite_sup) != num_genes or len(limite_inf) != num_genes:
                raise Exception("Los limites deben corresponder a la cantidad de genes del individuo")
            else:
                self.limites_sup = limite_sup
                self.limites_inf = limite_inf
        else:
            self.limites_sup = [limite_sup for _ in range(num_genes)]
            self.limites_inf = [limite_inf for _ in range(num_genes)]
        if repre == "discreta":
            self.cromosoma = [random.randint(self.limites_inf[i],self.limites_sup[i]) for i in range(num_genes)]
        elif repre == "real":
            self.cromosoma = [random.uniform(self.limites_inf[i],self.limites_sup[i]) for i in range(num_genes)]
        else:
            self.cromosoma = repre()

    def calcular_fitness(self, funcion_objetivo, optimizacion="minimizar", verbose=False):
        if not optimizacion in ["maximizar", "minimizar"]:
            raise Exception( "El argumento optimizacion debe ser: 'maximizar' o 'minimizar'")
        self.valor_funcion = funcion_objetivo(*self.cromosoma)
        if optimizacion == "maximizar":
            self.fitness = self.valor_funcion
        elif optimizacion == "minimizar":
            self.fitness = -(self.valor_funcion)

    def mutar(self, prob_mutar = 0.01, tipo="bit-flip", verbose=False):
        prob = np.random.uniform(low=0, high=1, size=self.num_genes)
        prob = prob_mutar > prob
        if tipo=="bit-flip":
            # Mutacion Bit-flip
            self.cromosoma = [1 ^ self.cromosoma[i] if v else self.cromosoma[i] for i,v in  enumerate(prob)]
        elif tipo == "intercambio":
            #Mutacion de Intercambio
            if prob_mutar > float(prob[0]):
                random_index = np.random.randint(len(self.cromosoma), size=2)
                self.cromosoma[random_index[0]], self.cromosoma[random_index[1]] = self.cromosoma[random_index[1]], self.cromosoma[random_index[0]]
                #self.cromosoma = self.cromosoma[:1]+partial_solution+self.cromosoma[-1:]
        elif tipo == "uniforme":
            factor_mutacion = np.random.uniform(low=-1,high=1,size=self.num_genes)
            self.cromosoma = [self.cromosoma[i]+factor_mutacion[i] if v else self.cromosoma[i] for i,v in enumerate(prob)]

        for i in range(self.num_genes):
            if self.cromosoma[i] < self.limites_inf[i]:
                self.cromosoma[i] = self.limites_inf[i]
            if self.cromosoma[i] > self.limites_sup[i]:
                self.cromosoma[i] = self.limites_sup[i]
        self.valor_funcion = None
        self.fitness = None

    def __repr__(self):
        texto = "Individuo" \
                + "\n" \
                + "---------" \
                + "\n" \
                + "Cromosoma: " + str(self.cromosoma) \
                + "\n" \
                + "Valor función objetivo: " + str(self.valor_funcion) \
                + "\n" \
                + "Fitness: " + str(self.fitness) \
                + "\n" \
                + "Límites inferiores de cada variable: " \
                + str(self.limites_inf) \
                + "\n" \
                + "Límites superiores de cada variable: " \
                + str(self.limites_sup) \
                + "\n"
        return(texto)


class Poblacion:

    def __init__(self, num_individuos, num_genes, limites_inf=None, limites_sup=None, repre="discreta", verbose = False):
        self.num_individuos = num_individuos
        self.num_genes = num_genes
        self.limites_inf = limites_inf
        self.limites_sup = limites_sup
        self.optimizado = False
        self.iter_optimizacion = None
        self.tiempo_optimizacion = None
        self.mejor_individuo = None
        self.mejor_fitness = None
        self.mejor_valor_funcion = None
        self.mejor_cromosoma = None
        self.historico_individuos = []
        self.historico_mejor_cromosomas = []
        self.historico_mejor_fitness = []
        self.historico_mejor_valor_funcion = []
        self.diferencia_abs = []
        # data.frame con la información del mejor fitness y valor de variables
        # encontrado en cada generación, así como la diferencia respecto a la
        # generación anterior.
        self.resultados_df = None
        self.fitness_optimo = None
        self.cromosoma_optimo = None
        self.valor_funcion_optimo = None
        self.individuos = [Individuo(num_genes,limites_sup,limites_inf,repre) for _ in range(num_individuos)]

        if verbose:
            print("----------------")
            print("Población creada")
            print("----------------")
            print("Número de individuos: " + str(self.num_individuos))
            print("Límites inferiores de cada variable: " \
                  + str(self.limites_inf))
            print("Límites superiores de cada variable: " \
                  + str(self.limites_sup))
            print("")

    def imprimir_individuos(self, n=None):
        if n is None:
            n = self.num_individuos
        elif n > self.num_individuos:
            n = self.num_individuos
        for i in range(n):
            print(self.individuos[i])

    def evaluar_poblacion(self, funcion_objetivo, optimizacion, verbose = False):
        for i in self.individuos:
            i.calcular_fitness(
                funcion_objetivo = funcion_objetivo,
                optimizacion = optimizacion,
                verbose = verbose
            )
        self.mejor_individuo = copy.deepcopy(self.individuos[0])
        for i in self.individuos:
            if i.fitness > self.mejor_individuo.fitness:
                self.mejor_individuo = i
        self.mejor_fitness = self.mejor_individuo.fitness
        self.mejor_valor_funcion = self.mejor_individuo.valor_funcion
        self.mejor_cromosoma = self.mejor_individuo.cromosoma
        if verbose:
            print("------------------")
            print("Población evaluada")
            print("------------------")
            print("Mejor fitness encontrado : " + str(self.mejor_fitness))
            print("Valor de la función objetivo: " \
                + str(self.mejor_valor_funcion))
            print("Mejor cromosoma encontrado : "
                + str(self.mejor_cromosoma))
            print("")

    def seleccionar_individuos(self, n, return_indices=False, metodo_seleccion="tournament", verbose=False):
        if metodo_seleccion not in ["ruleta", "rank", "tournament"]:
            raise Exception( "El método de selección debe de ser ruleta, rank o tournament")
        array_fitness = [copy.copy(i.fitness) for i in self.individuos]
        if metodo_seleccion == "ruleta":
            prob_selec = array_fitness / np.sum(array_fitness)
            index_seleccionado = np.random.choice(
                a    = np.arange(self.individuos),
                size = n,
                p    = list(prob_selec),
                replace = True
            )
        elif metodo_seleccion == "rank":
            order = np.flip(np.argsort(a=array_fitness) + 1)
            ranks = np.argsort(order) + 1
            probabilidad_seleccion = 1 / ranks
            probabilidad_seleccion = probabilidad_seleccion / np.sum(probabilidad_seleccion)
            index_seleccionado = np.random.choice(
                                a       = np.arange(self.num_individuos),
                                size    = n,
                                p       = list(probabilidad_seleccion),
                                replace = True
                            )
        elif metodo_seleccion == "tournament":
            index_seleccionado = []
            for _ in range(n):
                c_a = np.random.choice(a=np.arange(self.num_individuos),size=2,replace=False)
                c_b = np.random.choice(a=np.arange(self.num_individuos),size=2,replace=False)
                if array_fitness[c_a[0]] > array_fitness[c_a[1]]:
                    ganador_a = c_a[0]
                else:
                    ganador_a = c_a[1]

                if array_fitness[c_b[0]] > array_fitness[c_b[1]]:
                    ganador_b = c_b[0]
                else:
                    ganador_b = c_b[1]

                if array_fitness[ganador_a] > array_fitness[ganador_b]:
                    ind_final = ganador_a
                else:
                    ind_final = ganador_b

                index_seleccionado.append(ind_final)
        if verbose:
            print("----------------------")
            print("Individuo seleccionado")
            print("----------------------")
            print("Método selección: " + metodo_seleccion)
            print("")

        if(return_indices):
            return(index_seleccionado)
        else:
            if n == 1:
                return(copy.deepcopy(self.individuos[int(index_seleccionado)]))
            if n > 1:
                return(
                    [copy.deepcopy(self.individuos[i]) for i in index_seleccionado]
                )

    def cruzamiento(self, padre_1, padre_2, cruzamiento="un_corte", verbose=False):
        cromosoma_1, cromosoma_2 = [], []
        if cruzamiento == "un_corte":
            # Cruzamiento de un Punto de corte
            punto_cruce = np.random.randint(self.num_genes, size=1)[0]
            cromosoma_1 = padre_1.cromosoma[:punto_cruce] + padre_2.cromosoma[punto_cruce:]
            cromosoma_2 = padre_2.cromosoma[:punto_cruce] + padre_1.cromosoma[punto_cruce:]
        elif cruzamiento == "dos_cortes":
            # Cruzamiento de Dos Puntos de Corte
            puntos_cruce = np.random.randint(self.num_genes, size=2)
            punto_min , punto_max = min(puntos_cruce) , max(puntos_cruce)
            cromosoma_1 = padre_1.cromosoma[:punto_min] + padre_2.cromosoma[punto_min:punto_max] + padre_1.cromosoma[punto_max:]
            cromosoma_2 = padre_2.cromosoma[:punto_min] + padre_1.cromosoma[punto_min:punto_max] + padre_2.cromosoma[punto_max:]
        elif cruzamiento == "uniforme":
            # Cruzamiento uniforme
            mask = np.random.choice(a = [True, False], size = self.num_genes, replace = True)
            for i,v in enumerate(mask):
                if v:
                    cromosoma_1.append(padre_1.cromosoma[i])
                    cromosoma_2.append(padre_2.cromosoma[i])
                else:
                    cromosoma_1.append(padre_2.cromosoma[i])
                    cromosoma_2.append(padre_1.cromosoma[i])
        elif cruzamiento.upper() == "OX1":
            #Davis Order Crossover
            cromosoma_1 = [None for _ in padre_1.cromosoma]
            cromosoma_2 = [None for _ in padre_2.cromosoma]
            puntos_cruce = np.random.randint(self.num_genes, size=2)
            punto_min , punto_max = min(puntos_cruce) , max(puntos_cruce)
            cromosoma_1[punto_min:punto_max] = padre_1.cromosoma[punto_min:punto_max]
            cromosoma_2[punto_min:punto_max] = padre_2.cromosoma[punto_min:punto_max]
            for i,v in enumerate(padre_2.cromosoma):
                if v not in cromosoma_1:
                    cromosoma_1[i] = padre_2.cromosoma[i]
            for i,v in enumerate(padre_1.cromosoma):
                if v not in cromosoma_2:
                    cromosoma_2[i] = padre_1.cromosoma[i]
        hijo_1 = copy.deepcopy(padre_1)
        hijo_1.cromosoma = cromosoma_1
        hijo_2 = copy.deepcopy(padre_2)
        hijo_2.cromosoma = cromosoma_2
        return [hijo_1, hijo_2]

    def reemplazo_generacion(self, metodo_seleccion="tournament", prob_mutar=0.01, tipo_mutacion="bit-flip",
                            elitismo=0.0, verbose_seleccion=False, verbose_cruce=False, cruzamiento = "uniforme",
                            verbose_mutacion=False, verbose=False):
        nueva_generacion = []
        if elitismo > 0:
            # Número de individuos que pasan directamente a la siguiente
            # generación.
            n_elitismo = int(np.ceil(self.num_individuos*elitismo))

            # Se identifican los n_elitismo individuos con mayor fitness (élite).
            array_fitness = [copy.copy(i.fitness) for i in self.individuos]
            rank = np.flip(np.argsort(array_fitness))
            elite = [copy.deepcopy(self.individuos[i]) for i in rank[:n_elitismo]]
            # Se añaden los individuos élite a la lista de nuevos individuos.
            nueva_generacion = nueva_generacion + elite
        else:
            n_elitismo = 0

        while len(nueva_generacion)-n_elitismo < self.num_individuos:
            padres = self.seleccionar_individuos(n = 2, return_indices = False, metodo_seleccion = metodo_seleccion, verbose = verbose_seleccion)
            descendencia = self.cruzamiento(padre_1 = padres[0], padre_2 = padres[1],cruzamiento=cruzamiento,verbose = verbose_cruce)
            for d in descendencia:
                d.mutar(
                    prob_mutar  = prob_mutar,
                    tipo        = tipo_mutacion,
                    #distribucion     = distribucion,
                    #min_distribucion = min_distribucion,
                    #max_distribucion = max_distribucion,
                    verbose          = verbose_mutacion
                )
            nueva_generacion = nueva_generacion + [descendencia[0]]
            if len(nueva_generacion) >= self.num_individuos:
                break
            nueva_generacion = nueva_generacion + [descendencia[1]]

        self.individuos = copy.deepcopy(nueva_generacion)
        self.mejor_individuo = None
        self.mejor_fitness = None
        self.mejor_valor_variables = None
        self.mejor_valor_funcion = None
        # INFORMACIÓN DEL PROCESO (VERBOSE)
        # ----------------------------------------------------------------------
        if verbose:
            print("-----------------------")
            print("Nueva generación creada")
            print("-----------------------")
            print("Método selección: " + metodo_seleccion)
            print("Elitismo: " + str(elitismo))
            print("Número individuos élite: " + str(n_elitismo))
            print("Número de nuevos individuos: "\
                + str(self.num_individuos-n_elitismo))
            print("")

    def optimizar(self, funcion_objetivo, num_generaciones=10, optimizacion="maximizar",
                  metodo_seleccion="tournament", elitismo = 0.0, prob_mutar= 0.01, cruzamiento = "uniforme",
                  tipo_mutacion="bit-flip", rondas_tolerancia=10, stop_temprano = False,
                  tolerancia_parada = 0.1, verbose_evaluacion=False, verbose_nueva_gen=False,
                  verbose_seleccion=False, verbose_cruce=False, verbose_mutacion=False, verbose=False):
        start_time = time.time()
        for i in range(num_generaciones):
            if verbose:
                print("-------------")
                print("Generación: " + str(i))
                print("-------------")
            self.evaluar_poblacion(funcion_objetivo=funcion_objetivo, optimizacion=optimizacion, verbose=verbose_evaluacion)
            # Almacenar el historico
            self.historico_individuos.append(copy.deepcopy(self.individuos))
            self.historico_mejor_fitness.append(copy.deepcopy(self.mejor_fitness))
            self.historico_mejor_cromosomas.append(copy.deepcopy(self.mejor_cromosoma))
            self.historico_mejor_valor_funcion.append(copy.deepcopy(self.mejor_valor_funcion))
            #calculo de la diferencia absoluta respecto a la generacion anterior
            if i == 0:
                self.diferencia_abs.append(None)
            else:
                diferencia = abs(self.historico_mejor_fitness[i]/-self.historico_mejor_fitness[i-1])
                self.diferencia_abs.append(diferencia)
            #Criterio de STOP
            # Ocurre cuando la diferencia adsoluta entre el mejor individuo de cada generacion
            # no supera el valor de tolerancia de parada en las ultimas n generaciones
            # el valor de las n generaciones esta dado por el paramentro rondas_tolerancia
            if stop_temprano and i > rondas_tolerancia:
                ultimos_n_mejor_fitness = np.array(self.diferencia_abs[-(rondas_tolerancia):])
                if all(ultimos_n_mejor_fitness < tolerancia_parada):
                    if verbose:
                        print("Algoritmo detenido en la generación "
                            + str(i) \
                            + " por falta cambio absoluto mínimo de " \
                            + str(tolerancia_parada) \
                            + " durante " \
                            + str(rondas_tolerancia) \
                            + " generaciones consecutivas.")
                    break
            #Crear una nueva generacion
            self.reemplazo_generacion(
                metodo_seleccion=metodo_seleccion,
                elitismo=elitismo,
                prob_mutar=prob_mutar,
                tipo_mutacion = tipo_mutacion,
                cruzamiento = cruzamiento,
                verbose=verbose_nueva_gen,
                verbose_seleccion=verbose_seleccion,
                verbose_cruce=verbose_cruce,
                verbose_mutacion=verbose_mutacion)
        end_time = time.time()
        self.optimizado = True
        self.iter_optimizacion = i+1
        self.tiempo_optimizacion = end_time - start_time
        # Datos del mejor individuo de todas las generaciones
        self.fitness_optimo = max(self.historico_mejor_fitness)
        self.valor_funcion_optimo = max(self.historico_mejor_valor_funcion)
        self.cromosoma_optimo = self.historico_mejor_cromosomas[self.historico_mejor_fitness.index(self.fitness_optimo)]
        #Resumen
        if verbose:
            print("-------------------------------------------")
            print("Optimización finalizada " \
                  + datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            print("-------------------------------------------")
            print("Duración optimización: " + str(end_time - start_time))
            print("Número de generaciones: " + str(self.iter_optimizacion))
            print("Cromosoma Optimo: " + str(self.cromosoma_optimo))
            print("Valor función objetivo: " + str(self.valor_funcion_optimo))
            print("")

    def __repr__(self):
        texto = "============================" \
                + "\n" \
                + "         Población" \
                + "\n" \
                + "============================" \
                + "\n" \
                + "Número de individuos: " + str(self.num_individuos) \
                + "\n" \
                + "Límites inferiores de cada variable: " + str(self.limites_inf) \
                + "\n" \
                + "Límites superiores de cada variable: " + str(self.limites_sup) \
                + "\n" \
                + "Optimizado: " + str(self.optimizado) \
                + "\n" \
                + "Iteraciones optimización (generaciones): " \
                     + str(self.iter_optimizacion) \
                + "\n" \
                + "\n" \
                + "Información del mejor individuo:" \
                + "\n" \
                + "----------------------------" \
                + "\n" \
                + "Cromosoma: " + str(self.mejor_cromosoma) \
                + "\n" \
                + "Fitness: " + str(self.mejor_fitness) \
                + "\n" \
                + "\n" \
                + "Resultados tras optimizar:" \
                + "\n" \
                + "--------------------------" \
                + "\n" \
                + "Cromosoma óptimo: " + str(self.cromosoma_optimo) \
                + "\n" \
                + "Valor óptimo función objetivo: " + str(self.valor_funcion_optimo) \
                + "\n" \
                + "Fitness óptimo: " + str(self.fitness_optimo)
        return(texto)