import unittest
import mochila
import copy
import mock
import viajero

class mock_struct(object):
    def __init__(self, *args, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)
    def get(self, k, default=None):
        try:
            return getattr(self,k)
        except AttributeError:
            return default

class mock_item(mock_struct):
    pass

class MochilaTests(unittest.TestCase):

    def setUp(self):
        self.items = [
            mock_item(id=0, valor_item=1, peso_item=1),
            mock_item(id=1, valor_item=2, peso_item=2),
            mock_item(id=2, valor_item=3, peso_item=3),
            mock_item(id=3, valor_item=4, peso_item=4),
            mock_item(id=4, valor_item=5, peso_item=5)
        ]

    def test_initial_solution(self):
        m = mochila.Mochila(10, self.items, 100)
        r = m.initial_solution()
        #print(r)

    def test_random_neighbor(self):
        m = mochila.Mochila(1, self.items, 100)
        d = {"binary":[0,0,0,0,0], "obj":[]}
        r = m.random_neighbor(d)

    @mock.patch('mochila.Mochila.initial_solution')
    def test_solve(self, mock_solucion):
        m = mochila.Mochila(12, self.items, 10)
        mock_solucion.return_value = {"binary":[0,0,0,0,1], "obj":[]}
        m.solve_rmhc(False)

class ViajeroTests(unittest.TestCase):

    def setUp(self):
        self.matrix = [
            [0,8,4,1],
            [8,0,7,9],
            [4,7,0,5],
            [1,9,5,0]
        ]
        self.max_iter = 10

    def test_initial_solution(self):
        v = viajero.Viajero(matrix=self.matrix, max_iter=10)
        initial_solution = v.initial_solution()
        self.assertEqual(initial_solution[0], initial_solution[-1])

    def test_eval(self):
        v = viajero.Viajero(matrix=self.matrix, max_iter=10)
        solution = [0,1,2,3,0]
        length = v.eval(solution)
        self.assertEqual(length, 21)
        solution = [1,3,2,0,1]
        length = v.eval(solution)
        self.assertEqual(length, 26)

    def test_random_neighbour(self):
        v = viajero.Viajero(matrix=self.matrix, max_iter=10)
        solution = [0,1,2,3,0]
        neighbour = v.random_neighbour(solution)
        try:
            self.assertNotEqual(neighbour[1:-1],solution[1:-1])
        except AssertionError:
            pass
        self.assertEqual(neighbour[0], neighbour[-1])
        self.assertEqual(neighbour[0], solution[0])
        self.assertEqual(neighbour[-1], solution[-1])

    @mock.patch('viajero.Viajero.initial_solution')
    @mock.patch('viajero.Viajero.random_neighbour')
    def test_hill_climb(self, mock_nighbour, mock_initial_solution):
        mock_initial_solution.return_value = [1,3,2,0,1]
        mock_nighbour.return_value = [1,3,0,2,1]
        v = viajero.Viajero(matrix=self.matrix, max_iter=10)
        solution = v.hill_climb()
        self.assertEqual(solution[1],21)
        self.assertEqual(solution[0],[1,3,0,2,1])


if __name__ == "__main__":
    unittest.main()