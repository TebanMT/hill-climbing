# Copyright Esteban Mendiola Tellez All Rights Reserved.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file at root of this proyect or in
# https://gitlab.com/TebanMT/hill-climbing/-/blob/master/LICENSE
# ==============================================================================
# hill climbing search of a one-dimensional objective function
import numpy as np
from numpy import asarray
from numpy import arange
from numpy.random import randn
from numpy.random import rand
from numpy.random import seed
from matplotlib import pyplot

from pylab import meshgrid,cm,imshow,contour,clabel,colorbar,axis,title,show
from matplotlib.ticker import LinearLocator, FormatStrFormatter


class Funcion:

    def __init__(self):
        pass

    def evaluate(self, x):
        return abs(x**2.0+x-6)# x**3.0

class HillClimbing():

    def __init__(self, objetivo, limites, n_iteraciones, step_size):
        self.objetivo = objetivo
        self.limites = limites
        self.n_iteraciones = n_iteraciones
        self.step_size = step_size

    def solve(self):
        solucion = bounds[:, 0] + rand(len(bounds)) * (bounds[:, 1] - bounds[:, 0])
        solucion_eval = self.objetivo(solucion[0])
        soluciones = list()
        soluciones.append(solucion[0])
        for i in range(n_iterations):
            # take a step
            candidato = solucion + randn(len(bounds)) * step_size
            # evaluate candidate point
            candidato_eval = self.objetivo(candidato[0])
            # check if we should keep the new point
            if candidato_eval <= solucion_eval:
                # store the new point
                solucion, solucion_eval = candidato, candidato_eval
                # keep track of solutions
                soluciones.append(solucion[0])
                # report progress
                print('>%d f(%s) = %.5f' % (i, solucion[0], solucion_eval))
        return [solucion[0], solucion_eval, soluciones]

f = Funcion()


# seed the pseudorandom number generator
#seed(5)
# define range for input
bounds = asarray([[-10.0, 10.0]])
# define the total iterations
n_iterations = 1000
# define the maximum step size
step_size = 0.1
# perform the hill climbing search
h = HillClimbing(f.evaluate, bounds, n_iterations, step_size)
best, score, solutions = h.solve()
print('Done!')
print('f(%s) = %f' % (best, score))
# sample input range uniformly at 0.1 increments
inputs = arange(bounds[0,0], bounds[0,1], 0.1)

pyplot.plot(inputs, [f.evaluate(x) for x in inputs], '--')
pyplot.axvline(x=[0.0], ls='--', color='red')
pyplot.plot(solutions, [f.evaluate(x) for x in solutions], 'o', color='black')


#best, score, solutions = hillclimbing(objective, bounds, n_iterations, step_size)





#r = 6
#x = np.linspace(-r,r,1000)
#y = np.linspace(-r,r,1000)
#x = arange(-r,r,1)
#y = arange(-r,r,1)

#import math
#def objective(x,y):
#        return np.sin(x)+np.cos(y)

#X,Y = meshgrid(inputs, inputs)




#print(best)
#print(type(inputs))
#print(solutions)
#print(np.array(solutions[0]))

#fig = pyplot.figure()
#ax = fig.gca(projection='3d')
#Z = objective(X, Y)
#ax = fig.add_subplot(111, projection='3d')
#print(Z)
#surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,cmap=cm.RdBu,linewidth=0, antialiased=False)
#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

#ax.scatter(solutions[0], solutions[1], [objective(solutions[0][i],solutions[1][i]) for i,x in enumerate(solutions[0])], c='g', marker='o')


#fig.colorbar(surf, shrink=0.5, aspect=5)


#X,Y = meshgrid(np.array(solutions[0]), np.array(solutions[1]))
#Z = objective(X, Y)
#print(Z)
#surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,cmap=cm.RdBu,linewidth=10, antialiased=False)
#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
#fig.colorbar(surf, shrink=0.5, aspect=1)

# draw a vertical line at the optimal input
#pyplot.axvline(x=[0.0], ls='--', color='red')
# plot the sample as black circles

#pyplot.plot(solutions, [objective(x) for x in solutions], 'o', color='black')
#pyplot.show()

pyplot.show()
