/**
 * @license
 * Copyright 2021-2025 Esteban Mendiola Tellez All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at root of this proyect or in
 * https://gitlab.com/TebanMT/hill-climbing/-/blob/master/LICENSE
 */

/** TODO: Refactorizar codigo ya que contiene mucho codigo spaghetti y codigo duplicado
 *  Es valido separarlo en distintos archivos para una mejor lectura
 */

imgs = []
imgs_carrito = []
imgs_mochila = [] // Solucion
pesos = []
valores = []
execute = false;
p=0
v=0
solucion_items = []

//!!!!!---------------------------------------------------------------------!!!!!!!!!!!!

items = ["./img/1.png","./img/2.png","./img/3.png","./img/4.png","./img/5.png","./img/6.png","./img/7.png",
"./img/8.png","./img/9.png","./img/10.png","./img/infinity.png"]
var showItems = document.getElementById("bM");
showItems.innerHTML = 'Ver 0 Items';
var item_img = document.getElementById("img_item");

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var item_img = document.getElementById(data);
    const c = item_img.cloneNode(true)
    var peso = prompt("Ingrese el Peso del item:", "0");
    if (peso == null || peso == "" || peso == "0" ) {
        alert("Peso no valido")
    } else {
        var valor = prompt("Ingrese el Valor del item:", "0");
        if (valor == null || valor == "" || valor == "0") {
            alert("Valor no valido")
        }else {
            c.id = 'img_item'+imgs.length
            imgs.push(c);
            imgs_carrito.push(c)
            pesos.push(peso);
            valores.push(valor);
            ev.target.appendChild(c);
            showItems.innerHTML = 'Ver '+imgs.length+' Items';
            p = pesos.reduce((a, b) => parseFloat(a) + parseFloat(b));
            v = valores.reduce((a, b) => parseFloat(a) + parseFloat(b));
            changeItem(item_img)
            setDataCanasta({peso: p, valor:v})
        }
    }
}

function changeItem(item){
    item.src = items[0];
    if (items.length != 1) {
        items.splice(0,1);
    }
    div = document.getElementById("img_parent");
    div.appendChild(item)
}

function showModal() {
    const images = imgs.filter(Boolean);
    var table = $('<table class="table1"></table>').appendTo('.modal-body');
    for (let i = 0; i < Math.ceil(images.length/3); i++) {
        console.log("hola")
        table.append('<tr class=f'+i+'></tr>');
        j=i+1;
        for (const x of images.slice(i*3,j*3) ) {
            index = imgs.indexOf(x)
            $('.f'+i).append('<td><div class="iM" style="width: 50%;"><img style="width: 100%;" src="'+x.src+'"><p class="desc">Peso: '+pesos[index]+'</p><p class="desc">Valor: '+valores[index]+'</p></div></td>');
        }
    }
    $('#myModal').modal('show');
  }

function closeM(){
    $('.table1').remove();
}

//!!!!!---------------------------------------------------------------------!!!!!!!!!!!!

$('#execute').click(function(){
    if (pesos.length == 0 ) {
        alert("No tiene items en el carrito");
        return;
    }
    var peso_mochila = prompt("Ingrese el Peso Maximo de la mochila:", "0");
    if (peso_mochila == null || peso_mochila == "" || peso_mochila == "0" ) {
        alert("Peso no valido")
    } else {
        var iteraciones = prompt("Ingrese el Numero de iteraciones:", "0");
        if (iteraciones == null || iteraciones == "" || iteraciones == "0") {
            alert("Numero de iteraciones no valido")
        }else {
            for (const i of ANIMATIONS) {
                i.play()
            }
            $.ajax({
                url: '/',
                type: 'POST',
                cache: false, 
                data: { p: pesos, v: valores, p_m: peso_mochila, i: iteraciones }, 
                success: function(data){
                    solucion_items = data.solucion_items
                    setData(data);
                    fillImgsSolution(data);
                    execute = true;
                    r = document.getElementById("reiniciar");
                    r.disabled = false;
                    e = document.getElementById("execute");
                    e.disabled = true;
                    i = document.getElementById("img_parent");
                    i.style.display = "none";
                    d = document.getElementById("download");
                    d.disabled = false;
                    pm = document.getElementById('peso_max');
                    pm.innerHTML = "Peso Maximo: "+peso_mochila;
                    showItems.innerHTML = 'Ver '+(imgs.length - data.solucion_items.length)+' Items';
                    setDataCanasta({peso: (p - parseFloat(data.peso)), valor: (v - parseFloat(data.valor))});
                    for (const i of ANIMATIONS) {
                        i.pause()
                    }
                    what()
                }
                , error: function(jqXHR, textStatus, err){
                    alert('text status '+textStatus+', err '+err)
                    e = document.getElementById("execute");
                    e.disabled = false;
                    r = document.getElementById("reiniciar");
                    r.disabled = true;
                    d = document.getElementById("download");
                    d.disabled = true;
                    for (const i of ANIMATIONS) {
                        i.pause()
                    }
                }
             })
        }
    }
});

function fillImgsSolution(data){
    items = data.solucion_items;
    for (const i of items) {
        //item = imgs_carrito.splice(i.Item, 1);
        imgs_mochila.push({"id":i.Item,"peso":i.Peso,"valor":i.Valor,'src':imgs[i.Item].currentSrc})
        delete imgs[i.Item]
    }
}

function showSolucion(){
    var table = $('<table class="table1"></table>').appendTo('.modal-body');
    for (let i = 0; i < Math.ceil(imgs_mochila.length/3); i++) {
        table.append('<tr class=f'+i+'></tr>');
        j=i+1;
        for (const x of imgs_mochila.slice(i*3,j*3) ) {
            $('.f'+i).append('<td><div class="iM" style="width: 50%;"><img style="width: 100%;" src="'+x.src+'"><p class="desc">Peso: '+x.peso+'</p><p class="desc">Valor: '+x.valor+'</p></div></td>');
        }
    }
    $('#myModal').modal('show');
}



var data = {
    labels: ['Peso','Valor'],
    datasets: [{
        data: [0,0],
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)'
        ],
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)'
        ],
        borderWidth: 2,
    }]
};

var options = {
    plugins: {
        legend: false,
        tooltip: false,
    },
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "black",
                beginAtZero: true
            }
        }],
        xAxes: [{
            ticks: {
                fontColor: "black",
                fontSize: 14,
                stepSize: 1,
                beginAtZero: true,
            }
        }]
    }
};

var chart = new Chart('chart-0', {
    type: 'bar',
    data: data,
    options: options
});

// eslint-disable-next-line no-unused-vars
function setData(data_res) {
    chart.data.datasets.forEach(function(dataset) {
        dataset.data = [data_res.peso,data_res.valor]
    });
    chart.update();
}

var data_canasta = {
    labels: ['Peso','Valor'],
    datasets: [{
        data: [0,0],
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)'
        ],
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)'
        ],
        borderWidth: 2,
    }]
};

var chart_canasta = new Chart('chart-1', {
    type: 'bar',
    data: data_canasta,
    options: options
});

function setDataCanasta(data_res) {
    chart_canasta.data.datasets.forEach(function(dataset) {
        dataset.data = [data_res.peso,data_res.valor]
    });
    chart_canasta.update();
}

function reiniciar() {
    if (execute) {
        execute = false;
        r = document.getElementById("reiniciar");
        e = document.getElementById("execute");
        i = document.getElementById("img_parent");
        d = document.getElementById("download");
        i.style.display = "block";
        e.disabled = false;
        r.disabled = true;
        d.disabled = true;
        imgs = []
        imgs_carrito = []
        imgs_mochila = [] // Solucion
        pesos = []
        valores = []
        solucion_items = []
        showItems.innerHTML = 'Ver 0 Items';
        items = ["./img/1.png","./img/2.png","./img/3.png","./img/4.png","./img/5.png","./img/6.png","./img/7.png",
        "./img/8.png","./img/9.png","./img/10.png","./img/infinity.png"]
        setData({peso:0,valor:0});
        setDataCanasta({peso:0,valor:0});
        changeItem(item_img);
        reset()
    }
}

reiniciar()
changeItem(item_img)
