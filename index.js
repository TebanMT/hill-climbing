/**
 * @license
 * Copyright 2021-2025 Esteban Mendiola Tellez All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at root of this proyect or in
 * https://gitlab.com/TebanMT/hill-climbing/-/blob/master/LICENSE
 */

const express = require("express")
const fs = require('fs');
const download = require('download');
// Use python shell
var {PythonShell} = require('python-shell');

app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(__dirname+'/public'));


app.post('/', (req, res) => {
    pesos = req.body['p[]'].toString()
    valores = req.body['v[]'].toString()
    peso_mochila = req.body.p_m
    i = req.body.i
    var options = {
        mode: 'text',
        args: ['--valores='+valores,"--pesos="+pesos,"--mochila="+peso_mochila,"--iteraciones="+i]
    };
    PythonShell.run(__dirname+'/python/mochila.py', options, function (err, results) {
        if (err) throw err;
        python_response = JSON.parse(results[0].replace(/'/g, '"'));
        res.status(200).json(python_response);
    });
});

app.get('/download', (req, res) => {
    const path = `${__dirname}/public/reports/informe.txt`;
    res.download(path);
});

app.listen(process.env.PORT || 3000, () => {
    console.log(`Server started on port 3000`);
});